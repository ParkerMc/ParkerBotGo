.PHONY: help run build clean setup start-database

help: ## Show this help.
	@fgrep -h "##" $(MAKEFILE_LIST) | fgrep -v fgrep | sed -e 's/\\$$//' | sed -e 's/##//'

start-database: ## Start the database
	@echo Starting the database container

	mkdir -p run/mongo
	@if [ $(shell docker ps -a | grep -ci parkerbot-mongo) -eq 0 ]; then \
		docker run --name parkerbot-mongo -p 27017:27017 -d -v `pwd`/run/mongo:/data/db mongo --storageEngine wiredTiger > /dev/null; \
	elif [ $(shell docker ps | grep -ci parkerbot-mongo) -eq 0 ]; then \
		docker start parkerbot-mongo > /dev/null; \
	fi

stop-database: ## Stop the database
	@echo Stoping the database container

	@if [ $(shell docker ps -a | grep -ci parkerbot-mongo) -eq 1 ]; then \
		docker stop parkerbot-mongo > /dev/null; \
	fi

run: ## Run the bot
run: build
	@echo Running ParkerBot

	mkdir -p run
	rm -rf run/bin
	cp -r build/* run/

	cd run/; ENV=DEV ./bin/parkerbot


build: ## Build the bot
build: setup
	@echo Building ParkerBot

	rm -rf build
	mkdir -p build/bin/
	go build -o build/bin/parkerbot main.go

setup: ## Setup the project
	@echo Setting up the project

	go get ./...

clean: ## Clean up the build files and web app
clean: stop-database
	@echo Cleaning

	rm -rf build
	rm -rf run

	@if [ $(shell docker ps -a | grep -ci parkerbot-mongo) -eq 1 ]; then \
		docker rm -v parkerbot-mongo > /dev/null; \
	fi
