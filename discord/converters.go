package discord

import (
	"errors"
	"strconv"
	"time"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

func (client *Client) discordTypingStartConverter(typingStart *discordgo.TypingStart) model.TypingStart {
	return model.TypingStart{
		ChannelID: typingStart.ChannelID,
		Timestamp: time.Unix(int64(typingStart.Timestamp), 0),
		UserID:    typingStart.UserID,
	}
}

func (client *Client) discordAttachmentConverter(attachment *discordgo.MessageAttachment) model.MessageAttachment {
	return model.MessageAttachment{
		ID:   attachment.ID,
		Name: attachment.Filename,
		URL:  attachment.URL,
	}
}

func (client *Client) discordChannelConverter(channel *discordgo.Channel) model.Channel {
	var clientPtr model.Client = client
	convertedChannel := model.Channel{
		Client:   &clientPtr,
		ID:       channel.ID,
		Name:     channel.Name,
		ServerID: channel.GuildID,
		Topic:    channel.Topic,
		Dm:       false,
		PreEdits: make([]model.ChannelEdit, 0),
	}
	if channel.Type == discordgo.ChannelTypeDM {
		convertedChannel.Dm = true
	}
	return convertedChannel
}

func (client *Client) discordEmojiConverter(emoji *discordgo.Emoji) model.Emoji {
	return model.Emoji{
		ID:   emoji.ID,
		Name: emoji.Name,
	}
}

func (client *Client) discordGuildConverter(guild *discordgo.Guild) (model.Server, error) {
	var returnErr error
	convertedServer := model.Server{
		ID:          guild.ID,
		Name:        guild.Name,
		OwnerID:     guild.OwnerID,
		Icon:        guild.Icon,
		MemberCount: guild.MemberCount,
		Emojis:      make([]model.Emoji, 0),
		Members:     make([]model.ServerMember, 0),
		Channels:    make([]model.Channel, 0),
	}
	for _, emoji := range guild.Emojis {
		convertedServer.Emojis = append(convertedServer.Emojis, client.discordEmojiConverter(emoji))
	}
	for _, guildMember := range guild.Members {
		serverMember, err := client.discordGuildMemberConverter(guildMember)
		if err != nil {
			returnErr = err
			continue
		}
		convertedServer.Members = append(convertedServer.Members, serverMember)
	}
	for _, channel := range guild.Channels {
		convertedServer.Channels = append(convertedServer.Channels, client.discordChannelConverter(channel))
	}
	return convertedServer, returnErr
}

func (client *Client) discordGuildMemberConverter(guildMember *discordgo.Member) (model.ServerMember, error) {
	convertedMember := model.ServerMember{
		Nickname: guildMember.Nick,
		Joined:   guildMember.JoinedAt,
	}
	var err error
	convertedMember.User, err = client.discordUserConverter(guildMember.User)
	return convertedMember, err
}

func (client *Client) discordMessageConverter(message *discordgo.Message) (model.Message, error) {
	var returnErr error
	var clientPtr model.Client = client
	var editedTimestamp time.Time

	timestamp, err := message.Timestamp.Parse()
	if err != nil {
		returnErr = errors.New("Error phrasing the timestamp: " + err.Error())
		timestamp = time.Now()
	}
	if message.EditedTimestamp != "" {
		editedTimestamp, err = message.EditedTimestamp.Parse()
		if err != nil {
			returnErr = errors.New("Error phrasing the edited timestamp: " + err.Error())
			editedTimestamp = time.Now()
		}
	} else {
		editedTimestamp = timestamp
	}
	author, err := client.discordUserConverter(message.Author)
	if err != nil {
		returnErr = err
	}

	convertedMessage := model.Message{
		Author:          author,
		ChannelID:       message.ChannelID,
		Client:          &clientPtr,
		EditedTimestamp: editedTimestamp,
		ID:              message.ID,
		Msg:             message.Content,
		Timestamp:       timestamp,
		PreEdits:        make([]model.MessageEdit, 0),
	}

	if len(message.Attachments) > 0 {
		convertedMessage.Attachment = client.discordAttachmentConverter(message.Attachments[0])
	}
	return convertedMessage, returnErr
}

func (client *Client) discordReactionConverter(reaction *discordgo.MessageReaction) model.MessageReaction {
	var clientPtr model.Client = client
	return model.MessageReaction{
		ChannelID: reaction.ChannelID,
		Client:    &clientPtr,
		Emoji:     client.discordEmojiConverter(&reaction.Emoji),
		MessageID: reaction.MessageID,
		UserID:    reaction.UserID,
	}
}

func (client *Client) discordSnowflakeConverter(timestamp string) (time.Time, error) {
	snowflake, err := strconv.ParseUint(timestamp, 10, 0)
	if err != nil {
		return time.Now(), errors.New("Error phrasing snowflake: " + err.Error())
	}
	return time.Unix(int64(((snowflake>>22)+1420070400000)/1000), 0), nil
}

func (client *Client) discordUserConverter(user *discordgo.User) (model.User, error) {
	timestamp, err := client.discordSnowflakeConverter(user.ID)
	convertedUser := model.User{
		AvatarURL:    user.AvatarURL(""),
		Bot:          user.Bot,
		FullUsername: user.Username + "#" + user.Discriminator,
		ID:           user.ID,
		Timestamp:    timestamp,
		Username:     user.Username,
	}
	return convertedUser, err
}
