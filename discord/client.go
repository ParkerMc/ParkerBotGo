package discord

import (
	"errors"

	"github.com/bwmarrin/discordgo"
	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// Client the discord client for ParkerBot
type Client struct {
	config   Config
	handlers model.Handlers
	session  *discordgo.Session
}

// Init the driver
func (client *Client) Init(handlers model.Handlers) error {
	client.handlers = handlers
	var err error
	client.config, err = LoadConfig("configs/discord.json")
	client.session, err = discordgo.New("Bot " + client.config.Token)
	if err != nil {
		return errors.New("Error creating discord session: " + err.Error())
	}

	client.session.AddHandler(client.connectEvent)
	client.session.AddHandler(client.disconnectEvent)
	client.session.AddHandler(client.messageCreateEvent)
	client.session.AddHandler(client.messageDeleteEvent)
	client.session.AddHandler(client.messageDeleteReaction)
	client.session.AddHandler(client.messageReaction)
	client.session.AddHandler(client.messageUpdateEvent)
	client.session.AddHandler(client.typeingStartEvent)

	return nil
}

// Start starts the bot
func (client *Client) Start() error {
	err := client.session.Open()
	if err != nil {
		return errors.New("Error Connecting to the discord api: " + err.Error())
	}
	return nil
}

// Stop the driver connection
func (client *Client) Stop() error {
	err := client.session.Close()
	if err != nil {
		return err
	}
	return nil
}
