package handlers

import (
	"log"

	"gitlab.com/ParkerMc/ParkerBotGo/model"
)

// Handlers keeps the handlers in one place
type Handlers struct {
	channelHandlers               []func(*model.Channel)
	channelUpdateHandlers         []func(*model.Channel)
	channelDeleteHandlers         []func(*model.Channel)
	connectHandlers               []func(model.Client)
	database                      model.Database
	messageHandlers               []func(*model.Message)
	messageUpdateHandlers         []func(*model.Message)
	messageDeleteHandlers         []func(*model.Message)
	messageReactionHandlers       []func(*model.MessageReaction)
	messageReactionDeleteHandlers []func(*model.MessageReaction)
	typingStartHandlers           []func(*model.TypingStart)
}

// Create creates the handlers
func Create(database model.Database) *Handlers {
	return &Handlers{
		database: database,
	}
}

// RegisterConnectHandler registers a connect handler to the list
func (handlers *Handlers) RegisterConnectHandler(connectHandler func(model.Client)) {
	handlers.connectHandlers = append(handlers.connectHandlers, connectHandler)
}

// TriggerConnectHandler Triggers the connect handlers
func (handlers *Handlers) TriggerConnectHandler(client model.Client) {
	databseChannelList, err := handlers.database.GetChannelIDs()
	if err != nil {
		log.Print(err)
	}
	servers, err := client.GetServers()
	if err != nil {
		log.Print(err)
	}
	for _, server := range servers {
		log.Print(server.Name) // TODO register new servers
		for _, channel := range server.Channels {
			registered := false
			for _, channelID := range databseChannelList {
				if channel.ID == channelID {
					registered = true
				}
			}
			if !registered {
				handlers.database.AddChannel(&channel)
			}
		}
	}
	for _, connectHandler := range handlers.connectHandlers {
		connectHandler(client)
	}
}

// RegisterTypingStart registers a typing start handler to the list
func (handlers *Handlers) RegisterTypingStart(typingStartHandler func(*model.TypingStart)) {
	handlers.typingStartHandlers = append(handlers.typingStartHandlers, typingStartHandler)
}

// TriggerTypingStart Triggers the typing start handlers
func (handlers *Handlers) TriggerTypingStart(data *model.TypingStart) {
	for _, typingStartHandler := range handlers.typingStartHandlers {
		typingStartHandler(data)
	}
}
