package model

// Module model for all of the modules
type Module interface {
	Init(client *Client, handlers *Handlers)
}
