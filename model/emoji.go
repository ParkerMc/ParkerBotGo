package model

// Emoji holds data for each emoji
type Emoji struct {
	ID   string
	Name string
}
