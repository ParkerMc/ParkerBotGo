package model

import (
	"time"

	"gopkg.in/mgo.v2/bson"
)

// Channel stores channels
type Channel struct {
	Client     *Client       `bson:"-"`
	DatabaseID bson.ObjectId `bson:"_id,omitempty"`
	Dm         bool          `bson:"dm"`
	ID         string        `bson:"channelID"`
	Name       string        `bson:"name"`
	PreEdits   []ChannelEdit `bson:"pre_edits"`
	ServerID   string        `bson:"serverID"`
	Topic      string        `bson:"topic"`
}

// ChannelEdit used to keep track of edits
type ChannelEdit struct {
	Name      string    `bson:"name"`
	Timestamp time.Time `bson:"ts"`
	Topic     string    `bson:"topic"`
}
