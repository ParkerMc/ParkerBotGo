package model

// Database is the interface for the database
type Database interface {
	AddMessage(*Message) error
	AddChannel(*Channel) error
	Connect(*Config)
	Disconnect()
	GetChannel(string) (*Channel, error)
	GetChannelIDs() ([]string, error)
	GetMessage(string) (*Message, error)
	UpdateMessage(*Message) error
	UpdateChannel(*Channel) error
}
