package model

// Client model for the client
type Client interface {
	GetServers() ([]Server, error)
	Init(Handlers) error
	Start() error
	Stop() error
}
