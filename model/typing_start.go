package model

import "time"

// TypingStart used for when a user starts typing
type TypingStart struct {
	UserID    string
	ChannelID string
	Timestamp time.Time
}
