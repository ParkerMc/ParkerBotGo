package model

// Handlers stores the handlers
type Handlers interface {
	RegisterChannelHandler(func(*Channel))
	TriggerChannelHandler(*Channel)

	RegisterChannelDeleteHandler(func(*Channel))
	TriggerChannelDeleteHandler(string, Client)

	RegisterChannelUpdateHandler(func(*Channel))
	TriggerChannelUpdateHandler(*Channel)

	RegisterMessageHandler(func(*Message))
	TriggerMessageHandler(*Message)

	RegisterMessageDeleteHandler(func(*Message))
	TriggerMessageDeleteHandler(string, Client)

	RegisterMessageUpdateHandler(func(*Message))
	TriggerMessageUpdateHandler(*Message)

	RegisterMessageReactionHandler(func(*MessageReaction))
	TriggerMessageReactionHandler(*MessageReaction)

	RegisterMessageReactionDeleteHandler(func(*MessageReaction))
	TriggerMessageReactionDeleteHandler(*MessageReaction)

	RegisterConnectHandler(func(Client))
	TriggerConnectHandler(Client)

	RegisterTypingStart(func(*TypingStart))
	TriggerTypingStart(*TypingStart)
}
