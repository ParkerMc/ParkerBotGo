package mongo

import (
	"errors"

	"gitlab.com/ParkerMc/ParkerBotGo/model"

	"gopkg.in/mgo.v2/bson"
)

const messagesCollectionName string = "messages"

// AddMessage add a message to the database
func (mongo *Mongo) AddMessage(message *model.Message) error {
	return mongo.database.C(messagesCollectionName).Insert(message)
}

// GetMessage gets a message from the database by ID
func (mongo *Mongo) GetMessage(msgID string) (*model.Message, error) {
	count, err := mongo.database.C(messagesCollectionName).Find(&bson.M{"msgId": msgID}).Count()
	if err != nil {
		return nil, err
	}
	if count == 0 {
		return nil, errors.New("Message not found")
	}
	message := model.Message{}
	err = mongo.database.C(messagesCollectionName).Find(&bson.M{"msgId": msgID}).One(&message)
	if err != nil {
		return nil, err
	}
	return &message, nil
}

// UpdateMessage update a message in the database by ID
func (mongo *Mongo) UpdateMessage(message *model.Message) error {
	count, err := mongo.database.C(messagesCollectionName).Find(&bson.M{"_id": message.DatabaseID}).Count()
	if err != nil {
		return err
	}
	if count == 0 {
		return errors.New("Message not found")
	}

	err = mongo.database.C(messagesCollectionName).Update(&bson.M{"_id": message.DatabaseID}, message)
	if err != nil {
		return err
	}
	return nil
}
